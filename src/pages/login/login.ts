import { Component } from '@angular/core';
import {
  IonicPage,
  Loading,
  LoadingController,
  NavController,
  AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';

import firebase from 'firebase';


@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  nama:any;
  pass:any;

  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

    }


    loginButton()
    {
      // console.log(this.nama);
      let telur=firebase.auth().signInWithEmailAndPassword(this.nama,this.pass).catch(function(error) {
        // Handle Errors here.
      // var errorCode = error.code;
      // var errorMessage = error.message;
  // ...
});

telur.then(data=>{
  console.log("BERJAYA!",data);
  this.navCtrl.setRoot(HomePage);
});
    }
}
