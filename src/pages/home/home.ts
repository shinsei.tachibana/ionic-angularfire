import { Component } from '@angular/core';
import { NavController, AlertController ,NavParams,ActionSheetController} from 'ionic-angular';
import {LoginPage} from '../login/login';
import {UpdatePage} from '../update/update';

import firebase from 'firebase';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
appointment:FirebaseListObservable<any>;
  constructor(public navCtrl: NavController, public alertCtrl: AlertController,
    public navParams: NavParams, afAuth: AngularFireAuth,afD:AngularFireDatabase, public actionSheetCtrl: ActionSheetController) {
    this.navCtrl=navCtrl;
    this.appointment =afD.list('/book');
  }

  out()
  {
    let telur=firebase.auth().signOut().then(function() {
  // Sign-out successful.
}).catch(function(error) {
  // An error happened.
});

  telur.then(_=>{
    console.log("DAH KELUAR WEH!");
    this.navCtrl.setRoot(LoginPage);
  })

  }

  booking(){
    console.log("tekan button booking");
    let prompt = this.alertCtrl.create({
      title: 'Booking',
      message: "Enter an appointment detail in this form",
      inputs: [
        {
          name: 'date',
          type: 'date',
          placeholder: 'Date(DD/MM/YYYY)'
        },
        {
          name: 'location',
          placeholder: 'Location(Jalan No. 3, Taman Indah)'
        },
        {
          name: 'state',
          placeholder: 'State(Kuala Lumpur)'
        },
        {
          name: 'postcode',
          type: 'number',
          placeholder: 'Postcode(19000)'
        }
      ],
      buttons: [
        {
          text: 'Add',
          handler: data => {
            this.appointment.push({
              date: data.date,
              location: data.location,
              state:data.state,
              postcode:data.postcode,
              status:"new"
            });
          }
        },
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },

      ]
    });
    prompt.present();

  }
  update(){
    console.log("user tekan button update");
    this.navCtrl.push(UpdatePage);
  }
}
