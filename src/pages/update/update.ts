import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController , ActionSheetController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';

import firebase from 'firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
/**
 * Generated class for the BookingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html',
})
export class UpdatePage {
  books: FirebaseListObservable<any>;
  constructor(public navCtrl: NavController, public alertCtrl: AlertController,
    public navParams: NavParams, afAuth: AngularFireAuth,afD:AngularFireDatabase, actionSheetCtrl : ActionSheetController) {
    this.books=afD.list('/book');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatePage');
  }


  addBook(){
    let prompt = this.alertCtrl.create({
      title: 'Song Name',
      message: "Enter a name for this new song you're so keen on adding",
      inputs: [
        {
          name: 'date',
          type: 'date',
          placeholder: 'Date(DD/MM/YYYY)'
        },
        {
          name: 'location',
          placeholder: 'Location(Jalan No. 3, Taman Indah)'
        },
        {
          name: 'state',
          placeholder: 'State(Kuala Lumpur)'
        },
        {
          name: 'postcode',
          type: 'number',
          placeholder: 'Postcode(19000)'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            this.books.push({
              date: data.date,
              location: data.location,
              state:data.state,
              postcode:data.postcode,
              status:"new"
            });
          }
        }
      ]
    });
    prompt.present();
  }
  showOptions(bookId, bookDate) {
    let prompt= this.alertCtrl.create({
      title: 'What do you want to do?',
      buttons: [
        {
          text: 'Cancel Appontment',
          role: 'destructive',
          handler: () => {
            this.books.remove(bookId);
          }
        },{
          text: 'Update Date',
          handler: () => {
            this.updateBooks(bookId, bookDate);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    prompt.present();
  }
  updateBooks(bookId, bookDate){
  let prompt = this.alertCtrl.create({
    title: 'Update Appointment',
    message: "Update the detail for this appointment",
    inputs: [
      {
        name: 'date',
        placeholder: 'yyyy-mm-dd',
        type: 'date',
        value: bookDate
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Save',
        handler: data => {
          this.books.update(bookId, {
            date: data.date
          });
        }
      }
    ]
  });
  prompt.present();
}

  out()
  {
    let telur=firebase.auth().signOut().then(function() {
  // Sign-out successful.
  }).catch(function(error) {
  // An error happened.
  });

  telur.then(_=>{
    console.log("DAH KELUAR WEH!");
    this.navCtrl.setRoot(LoginPage);
  })

  }
}
